const express = require('express');

const server = express();

server.use(express.json());


const users = ['Léo', 'Karol'];

function checkExists( req, res, next){
    if(!req.body.name){
        return res.status(400).json({ error: 'Usuario é requisitado'})
    }

    return next();
}

function checkUserInArray( req, res, next){
    if(!users[req.params.index]){
        return res.status(400).json({ error: 'Usuarionão existe'})
    }

    req.user = user;

    return next();
}



server.get('/users', checkUserInArray, (req, res) => {
    return res.json(users);
})

server.get('/users/:index', checkUserInArray, (req, res) => {
    return res.json(req.user);
})

server.post('/users', checkExists, (req, res) => {
    const {name} = req.body;

    users.push(name);

    return res.jason(users);
});

server.put('/users/:index', checkExists, checkUserInArray, (req, res) => {
    const { index } = req.params;
    const { name } = req.body;

    users[index] = name;

    return res.json(users);
});

server.delete('/users/:index', checkUserInArray, (req, res) => {
    const { index } = req.params;

    users.splice(index, 1);

    return res.send();
});

server.listen(3000);